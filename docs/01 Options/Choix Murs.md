# Choix murs

Ce menu permet de choisir les murs qui seront affectés par les réservations, selon divers paramètres.

# GUIDE D'UTILISATION

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![Sélection Configuration](../images/ChoixMurs/ToolBar.PNG)

#### 2. La boite de dialogue suivante s'affiche :

![Boîte de Dialogue Choix Murs](../images/ChoixMurs/BoiteDialogue_ChoixMurs.PNG)

Le choix pour s'effectuer selon l'une des deux options proposées :
- <span style="color:red"><b>1</b></span> : En ciblant des murs avec une certaine épaisseur grâce à des valeurs limites.
- <span style="color:red"><b>2</b></span> : En choisissant spécifiquement les types de murs que l'on souhaite utiliser.
<br><br>

<span style="color:red"><b>Attention, ces deux options ne sont pas cumulables.</b></span>

#### 3. Une fois votre sélection effectuée, cliquez sur Valider