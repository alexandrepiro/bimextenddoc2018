# Choix Phases

Ce menu permet de définir les éléments qui seront affectés par les réservations en fonction des phases qui les concernent.

# GUIDE D'UTILISATION

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![Sélection Configuration](../images/ChoixMurs/ToolBar.PNG)

#### 2. La boite de dialogue suivante s'affiche :

![Boîte de Dialogue Choix Murs](../images/ChoixPhases/Phases_BoiteDialogue.PNG)

<br>Elle permet de définir les phases que l'on souhaite utiliser :
<br>
- <span style="color:red"><b>1</b></span> : Permet de définir la phase sur laquelle on souhaite attribuer les réservations : la phase du réseau pour lequel la réservation est créée, ou une phase dédiée.

- <span style="color:red"><b>2</b></span> : Paramétrer les règles pour les phases des réseaux. On peut ainsi créer les réservations pour les réseaux présents quelle que soit leur phase, exclure les réseaux concernés par une phase de démolition, ou bien n'utiliser que les réseaux appartenant à un ou des phases particulières.

- <span style="color:red"><b>3</b></span> : Sélection de phases similaires au <span style="color:red"><b>2</b></span> mais pour les éléments structurels.

#### 3. Une fois votre sélection effectuée, cliquez sur Valider
