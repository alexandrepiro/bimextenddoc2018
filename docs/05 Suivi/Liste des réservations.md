# Liste des réservations

La commande Liste des réservtaions va vous permttre d'obtenir un tableau récapitualtif des réservtaions présentes dans le projet.

#### 1) Cliquez sur l’outil dans le ruban PIRO CIE

![Toolbar Liste des réservations](../images/Toolbar_Suivi.png)

#### 2) La boîte de dialogue suivante s'affiche :

![Boîte de dialogue sélection](../images/ResaAuto/selobjects.png)

Vous pouvez choisir les réservations que vous souhaitez visualiser.
Vous pouvez visualiser, toutes les réservations, juste un niveau, les réservations présentes sur un sous-projet ou sélectionner celles que vous souhaitez.

#### 3) Le tableau des réservations s'affiche :

![Tableau Liste des réservations](../images/ListeResa/TableauListeResa.png)
<br>
<span style="color:#4286f4"><b>A.</b></span> Vous pouvez filtrer ou grouper les données :

![Filtrer les données](../images/ResaAuto/filtre.png)
![Grouper les données](../images/ResaAuto/groupe.png)

<br>
<span style="color:#4286f4"><b>B.</b></span> Vous pouvez modifier directement la forme de la ou des réservations sélectionnées, et y ajouter un commentaire.

<br>
<span style="color:#4286f4"><b>B.</b></span> Exporter les données du tableau sous format CSV.

Pour chaque ligne, vous pouvez également modifier la forme de la réservations et ajouter/modifier/supprimer un commentaire directement depuis les cellules du tableau.

![Liste Résa : Modifier la forme](../images/ListeResa/liste2.png)
<br>
![Liste Résa : Modifier un commentaire](../images/ListeResa/liste3.png)

#### 4) Cliquez sur Valider pour enregistrer les modifications :

![Liste Résa : Propriétés avant modification](../images/ListeResa/liste4.png)
![Liste Résa : Propriétés après modification](../images/ListeResa/liste5.png)