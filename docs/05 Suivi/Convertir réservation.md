# Convertir Réservation

Cette commande va vous permettre de modifier le type d'une réservation. Vous allez pouvoir transformer une réservation GOE (vide) en MEP (pleine) et vice versa.

<span style="color:red"><b>Attention, pour que les réservations GOE coupent la matière, les éléments structurels (Dalles, Murs, Poutres) doivent être présent dans le document et non en lien.</b></span>

# GUIDE D'UTILISATION

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![Toolbar Suivi](../images/Toolbar_Suivi.png)

#### 2. La boîte de dialogue suivante s'affiche :

![Boîte de dialogue Conversion](../images/Conversion/convert1.png)

Vous pouvez choisir les réservations que vous souhaitez importer.
Soit toutes les réservations, juste celles présentes sur un niveau, ou dans un sous-projet, ou séléctionner celles que vous souhaitez. Si les réservations sont dans un lien, la partie "sous-projets" ne sera active que si ce lien est un fichier partagé (travail collaboratif). 

<span style="color:red"><b>Dans les Options, pour cette commande, les réservations sont recherchées dans les liens Réseaux.</b></span>

#### 3. Sélectionnez le type de réservations que vous souhaitez puis cliquez sur Valider :

![Choix Conversion](../images/Conversion/convert2.png)

Les réservations sont modifiées : 
<br>
![Insertion réservations importées](../images/Conversion/convert3.png "Avant")
![Insertion réservations importées](../images/Conversion/convert4.png "Après")


