# Importer depuis un lien

La commande <i>Importer depuis un lien</i> permet d'importer les réservations présentes dans un lien. Ces réservations doivent avoir été créées avec les commandes du ruban PIRO CIE

<span style="color:red"> Dans les Options, il faut cocher liens réseaux et sélectionner le lien contenant les réservations.</span>

## GUIDE D'UTILISATION

#### 1) Cliquez sur l’outil dans le ruban PIRO CIE

![Toolbar Suivi](../images/Toolbar_Suivi.png)

#### 2) La boîte de dialogue suivante s'affiche :

![Toolbar Suivi](../images/Conversion/convert1.png)

Vous pouvez choisir les réservations que vous souhaitez importer.
Soit toutes les réservations, juste celles présentes sur un niveau, ou dans un sous-projet, ou séléctionner celles que vous souhaitez. Si les réservations sont dans un lien, la partie "sous-projets" ne sera active que si ce lien est un fichier partagé (travail collaboratif). 

<span style="color:red"><b>Dans les Options, pour cette commande, les réservations sont recherchées dans les liens Réseaux.</b></span>

#### 3) Sélectionnez le type de réservations que vous souhaitez puis cliquez sur Valider :

![Choix Conversion](../images/Conversion/convert2.png)

Les réservations sont insérées dans le projet : 
<br>
![Insertion réservations importées](../images/Import/import2.png "Avant")
![Insertion réservations importées](../images/Import/import3.png "Après")
