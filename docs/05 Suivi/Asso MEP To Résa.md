# Associer les réseaux à leur réservations

Cette commande permet de lier les réservations aux réseaux qui les traversent. Cette manipulation est nécessaire pour permettre la mise en oeuvre de la commande Position Automatique.

## Guide d'utilisation

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![ToolBar position auto](../images/PosAuto/ToolBar.PNG)

#### 2. La boîte de dialogue suivante s'affiche :

![ResaAuto Choix des Réseaux](../images/ResaAuto/selobjects.PNG)

Vous pouvez choisir les réservations pour lesquelles vous souhaitez réaliser l'association.
<br><br>
Plusieurs choix de sélection existent : 

- L'ensemble des réservations présentes dans le projet
- Les réservations d'un niveau défini
- Des réservations définies manuellement