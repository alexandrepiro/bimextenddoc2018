# Modifier les réservations

Cette commande va vous permettre d'obtenir des informations sur la réservation choisie mais également de modifier les valeurs de certains de ses paramètres telles que sa forme, l'épaisseur de rebouchage etc.

## Guide d'utilisation

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![Modification Résa toolbar](../images/Modification/Toolbar_Modification.PNG)

#### 2. Sélectionnez la réservation pour laquelle vous souhaitez obtenir les paramètres en cliquant dessus.

#### 3. La boîte de dialogue suivante apparaît :

![Boîte de dialogue Modification Résa](../images/ModifResa/dial.PNG)

<span style="color:red"><b>1.</b></span> Valeurs des paramètres non modifiables 
<br>
<span style="color:red"><b>2.</b></span> Paramètres modifiables de la réservation
<br>
<span style="color:red"><b>2.</b></span> Informations sur le Réseau et le Support de la réservation. Les données sur la composition du support peuvent être accessibles depuis le bouton <span style="color:#4286f4"><b>A</b></span>  
<br>
<span style="color:red"><b>Attention : Lorsque la réservation est traversée par plusieurs réseaux, les informations sur le réseau ne sont pas affichées, ainsi que l'épaisseur de rebouchage qui ne peut alors être modifiée.</b></span>





